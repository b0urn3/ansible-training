# Scenario 5

Very typical situation when you get yourself into the point that your automation is starting to take off and you want to solve more problems on less space or get stuff implemented generally.

This scenario presents one of the nice examples that are useful when you need to have a generic task that is taking a variable which name is constructed on the fly.

Therefore create a playbook that is capable of printing a variable value but the name of the variable is constructed from the value of another variable.
