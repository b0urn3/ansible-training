# Scenario 8

You are managing a configuration file by using a template module and pass variables to generate the proper settings for each deployment.

Your very important service is expecting a parameter in configuration file that has to be set to "true" and nothing else. Otherwise your service will fail to start.

You have implemented the playbook and Jinja template. To make really sure that your very important service will have its configuration file created correctly you have added an additional task that checks that the this single most important setting is correct.

**You expect by running your playbook that the only changed task will be the template one, the second will be green!** However you notice that this is not the case :(.

What is going on? Why is your template not working and what needs to be changed to make it stable a reliable?

# Solution

Ansible has several keywords that it interprets as they are interpreted by Python programming language. One of such keywords is `true` that is used for boolean values.

Ansible recognizes `yes`, `no`, `true`, `false` as valid boolean values. When you pass these as template variables that should become string then Python function that handles converting from boolean to string will interpret them and transfor to strings `True` or `False`.

This will fail the check in the second task and it will correct it.

To fix this issue you have to explicitly tell Ansible that this value is **string** already and shouldn't be interpreted anymore.
