# Scenario 7

When you need to move files between hosts (which is totally possible) but you don't want to or can't store a copy of such files on the controllers filesystem, you may endup using [slurp](https://docs.ansible.com/ansible/latest/slurp_module.html) module.

This module is tempting because it fetches the file(s) into memory and then allows you to upload them to the desired host in the next task.

But there is a caveat to that.

How would you fix this playbook and holding on to the above requirement of not storing the file locally on controller ([fetch](https://docs.ansible.com/ansible/latest/fetch_module.html#fetch) module is not an option!)

To create a large file you can use following command

```bash
dd if=/dev/zero of=file.txt count=1024 bs=1048576
```

# Solution

You can use few approaches here

* In Ansible there is another module called [synchronized](https://docs.ansible.com/ansible/latest/modules/synchronize_module.html) that can be used but it **requires** `rsync` to be installed on both sides of the transaction

* You can use `shell` module and `scp`
* Or have an network storage that you can leverage
* Any other options?

