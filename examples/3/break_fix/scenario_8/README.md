# Scenario 9

You have all your automation implemented and were using Ansible Engine for some time now. Now you want to move to Tower but find out that some playbooks are not being recognized in the UI.

# Solution

This was a bug in Ansible Tower before release 3.3.0. In those versions a playbook that didn't contain `hosts` keyword wasn't recognized as a valid playbook but when it was manually searched for and picked everything worked.

This scenario is just a very simple example that sometimes working solution implemented just by using Ansible Engine doesn't have to work when used in Tower. Other differences can be found in the future.

