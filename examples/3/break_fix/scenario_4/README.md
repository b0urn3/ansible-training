# Scenario 4

Fact gathering optimization is one of the basic optimizations that you can do you increase the speed of exection. Many time fact gathering can be disabled altogether.

In this scenario our playbook needs 2 facts to function but our optimization wasn't correct. What needs to be chagned?

# Solution

First our subset exclusion expression makes all fact gathering disabled. It's the same as if we just left `gather_facts: no`.

Second the addtional filter we are setting is disabling the second fact we need.
